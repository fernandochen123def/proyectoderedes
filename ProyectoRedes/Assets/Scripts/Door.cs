using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour,IInteractuable
{
    Animator animator;
    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    public void Interact()
    {
        animator.SetTrigger("OpenDoor");
    }
}
