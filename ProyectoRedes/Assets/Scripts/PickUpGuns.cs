using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpGuns : MonoBehaviour, IInteractuable
{
    public static PickUpGuns singleton { get; private set; }

    public enum GunType { Rifle, Shotgun, Pistol, Sword}
    [SerializeField] public GunType type;
    [SerializeField] private float gunDMG;
    [SerializeField] private float gunAmmoActual;
    [SerializeField] private float gunMaxAmmo;
    [SerializeField] private float gunReloadTime;
    [SerializeField] private float gunVelocityMovement;
    public int gunIndex;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Interact()
    {
        if (type == GunType.Rifle)
        {
            gunIndex = 0;
        }
        if (type == GunType.Pistol)
        {
            gunIndex = 1;

        }
        if (type == GunType.Sword)
        {
            gunIndex = 2;

        }
    }
    
}
