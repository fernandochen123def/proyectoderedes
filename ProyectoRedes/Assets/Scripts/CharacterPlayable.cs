using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPlayable : MonoBehaviour
{

    [Header("Weapons")]
    [Tooltip("Weapon Index Array")]
    [SerializeField] GameObject[] weapons;
    [SerializeField] int GunWeapon;

    [Header("Animator")]
    Animator animator;
    [SerializeField] string aim = "Aim";
    [SerializeField] string dash = "Dash";
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void ChangeWeapon( int weapon)
    {
        
         for (int i = 0; i < weapons.Length; i++)
         {

            if (weapon == i)
            {
                   weapons[weapon].SetActive(true);
            }
            else
            {
                weapons[i].SetActive(false);

            }


        }


    }
    #region Coroutines
    private void OnTriggerStay(Collider other)
    {
            if (other.gameObject.CompareTag("Door"))
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (other.TryGetComponent(out IInteractuable interactuableObject))
                    {
                        interactuableObject.Interact();
                     }
                }
            }
        
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUpWeapon"))
        {
            if (other.TryGetComponent(out IInteractuable interactuableObject))
            {
                interactuableObject.Interact();
                GunWeapon = other.GetComponent<PickUpGuns>().gunIndex;
                ChangeWeapon(GunWeapon);
            }
        }
    }
    #endregion
}
